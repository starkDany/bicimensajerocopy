class Pide < MailForm::Base
  attribute :name,      :validate => true
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :telefono, :validate => true
  attribute :pickup, :validate => true
  attribute :dropoff, :validate => true
  attribute :comentario, :validate => true
  attribute :precio, :validate => true
  attribute :distancia, :validate => true
  attribute :nickname,  :captcha  => true

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => "Cotizacion nueva",
      :to => "bicimensajero420@gmail.com",
      :cc => %(<#{email}>),
      :bcc => "danielvp1987@gmail.com",
      :from => %("#{name}" <#{email}>)
    }
  end
end