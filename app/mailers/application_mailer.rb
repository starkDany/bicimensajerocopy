class ApplicationMailer < ActionMailer::Base
  default from: "noresponder@bicimensajero.com"
  layout 'mailer'
end
