class ContactsController < ApplicationController
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    @contact.request = request
    if @contact.deliver
      redirect_to root_path, notice: "Recibimos tu mensaje, en menos de 14 minutos recibes tu cotización"
    else
      flash.now[:error] = 'No se pudo enviar tu mensaje, por favor marcanos al: 55-555-555'
      render :new
    end
  end
end