class PidesController < ApplicationController
  def new
    @pide = Pide.new
  end

  def create
    @pide = Pide.new(params[:pide])
    @pide.request = request
    if @pide.deliver
      redirect_to root_path, notice: "Recibimos tu solicitud, tu bicimensajeo está en camino, si tienes dudas, estamos disponibles en este número: 55-3555-1151"
    else
      flash.now[:error] = 'No se pudo enviar tu cotizacion, por favor marcanos al: 55-3555-1151'
      render :new
    end
  end
end